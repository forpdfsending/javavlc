package javavlctest;

import java.awt.EventQueue;
import java.io.File;

import javax.swing.JFrame;

import com.sun.jna.Native;
import com.sun.jna.NativeLibrary;

import uk.co.caprica.vlcj.binding.LibVlc;
import uk.co.caprica.vlcj.binding.RuntimeUtil;
import uk.co.caprica.vlcj.player.component.EmbeddedMediaPlayerComponent;
import uk.co.caprica.vlcj.player.embedded.EmbeddedMediaPlayer;

public class MainForm {

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainForm window = new MainForm();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public MainForm() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
//		
//		JButton btnNewButton = new JButton("New button");
//		btnNewButton.addActionListener(new ActionListener() {
//			public void actionPerformed(ActionEvent arg0) {
//				System.out.println("rgdfgdfgdf");
//			}
//		});
//		
//		JLabel lblNewLabel = new JLabel("New label");
//		GroupLayout groupLayout = new GroupLayout(frame.getContentPane());
//		groupLayout.setHorizontalGroup(
//			groupLayout.createParallelGroup(Alignment.LEADING)
//				.addGroup(groupLayout.createSequentialGroup()
//					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
//						.addGroup(groupLayout.createSequentialGroup()
//							.addGap(18)
//							.addComponent(btnNewButton))
//						.addGroup(groupLayout.createSequentialGroup()
//							.addGap(176)
//							.addComponent(lblNewLabel)))
//					.addContainerGap(212, Short.MAX_VALUE))
//		);
//		groupLayout.setVerticalGroup(
//			groupLayout.createParallelGroup(Alignment.LEADING)
//				.addGroup(groupLayout.createSequentialGroup()
//					.addGap(26)
//					.addComponent(btnNewButton)
//					.addGap(81)
//					.addComponent(lblNewLabel)
//					.addContainerGap(117, Short.MAX_VALUE))
//		);
//		frame.getContentPane().setLayout(groupLayout);
		
		  File vlcInstallPath = new File("C:\\Program Files\\VideoLAN\\VLC");
		  
		  NativeLibrary.addSearchPath(
	                RuntimeUtil.getLibVlcLibraryName(), vlcInstallPath.getAbsolutePath());
	  //      Native.loadLibrary(RuntimeUtil.getLibVlcLibraryName(), LibVlc.class);
	        //LibXUtil.initialise();
		
		
		EmbeddedMediaPlayerComponent	mediaPlayerComponent = new EmbeddedMediaPlayerComponent();

		frame.setContentPane(mediaPlayerComponent);

		frame.setLocation(0, 0);
		frame.setSize(300,400);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
		
		
		EmbeddedMediaPlayer q  =mediaPlayerComponent.mediaPlayer();	
		q.media().play("rtsp://170.93.143.139/rtplive/470011e600ef003a004ee33696235daa");
	}
}
